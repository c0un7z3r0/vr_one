import React from 'react';
import {
  AmbientLight,
  AppRegistry,
  asset,
  StyleSheet,
  SpotLight,
  PointLight,
  Pano,
  Text,
  View,
  Box,
} from 'react-vr';

export default class VR_one extends React.Component {
  render() {
    return (
      <View>
        <Pano source={asset('sugarloaf.jpg')}/>
        <AmbientLight intensity={ 0.2 }  />

          <SpotLight
              decay={2}
              penumbra={0.616}
              angle={80} intensity={ 3 }
              style={{
                transform: [{translate: [0, 5, -16]}]
              }}
              />


        <Box
          dimWidth={1}
          dimDepth={1}
          dimHeight={1}
          shadow={1}
          style={{
            color:'#0000FF',
            transform: [{translate: [0, -0.1, -4]}]
          }}
          lit
          shadow
        />

        <Box
          dimWidth={1}
          dimDepth={1}
          dimHeight={1}
          style={{
            color:'#00FF00',
            transform: [{translate: [-2, -0.1, -4]}]
          }}
          lit
          />

        <Box
          dimWidth={1}
          dimDepth={1}
          dimHeight={1}
          style={{
            color:'#FF0000',
            transform: [{translate: [-4, -0.1, -4]}]
          }}
          lit
          />


        <Box
          dimWidth={0.1}
          dimDepth={0.1}
          dimHeight={100}
          style={{
            color:'#000000',
            transform: [{translate: [-0.05, -1, -0.05]}]
          }}
          lit
          />
        <Box
          dimWidth={100}
          dimDepth={100}
          dimHeight={0.5}
          texture={asset('check_floor_tile.jpg')}
          style={{
            color:'#333333',
            transform: [{translate: [0, -1.7, 0]}]
          }}
          lit
          />
        <Text
          style={{
            backgroundColor: '#777879',
            fontSize: 0.8,
            fontWeight: '400',
            layoutOrigin: [0.5, 0.5],
            paddingLeft: 0.2,
            paddingRight: 0.2,
            textAlign: 'center',
            textAlignVertical: 'center',
            transform: [{translate: [0, 0, -10]}]
          }}
          lit>
          VR_one MOFO
        </Text>
      </View>
    );
  }
};

AppRegistry.registerComponent('VR_one', () => VR_one);
